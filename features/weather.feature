Feature: Check if the weather service works properly
  In order to check the weather service
  As beginner
  I'll get some values and check if they are ok and if the temperature given is correct

  Scenario: Check if a city and a country given are correct
    Given I access the url API
    And the city is "London" and the country "GB"
    When I ask for the city and country name
    Then I check if the city and country are correct
    And I check if the status code of the page look by city and country is "200"