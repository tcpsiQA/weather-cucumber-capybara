require 'json'
require 'uri'
require 'net/http'

class Weather
  include Capybara::DSL
  include RSpec::Matchers
  def getUrl(city,country)
    visit "http://api.openweathermap.org/data/2.5/weather"+'?q='+ city + ',' + country
    @json = JSON.parse(find("pre").text)
  end

  def getInfo(city,country)
    expect(@json["name"]).to eq(city)
    expect(@json["sys"]["country"]).to eq(country)
  end

  def haveCode(cod)
    expect(@json["cod"]).to eq(cod.to_i)
  end
end