require_relative('weather')
weather=Weather.new

Given(/^I access the url API$/) do
end

And(/^the city is "([^"]*)" and the country "([^"]*)"$/) do |city,country|
  @city=city
  @country=country

end

When(/^I ask for the city and country name$/) do
  weather.getUrl(@city,@country)
end

Then(/^I check if the city and country are correct$/) do
  weather.getInfo(@city,@country)
end

And(/^I check if the status code of the page look by city and country is "([^"]*)"$/) do |cod|
  weather.haveCode(cod)
end